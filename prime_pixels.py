"""
This module contains functions for generating an image filled with prime numbers.

The `generate_primes` function:
    generates a list of prime numbers up to a given number.
The `create_image` function:
    creates an image of a specified shape (circle or rectangle) filled with prime numbers.
The color of each pixel in the image is determined by whether its index is a prime number.

This script can be run directly from the command line, and the user can specify the shape,
width, and height of the image.
"""

import math
import argparse
from typing import List
from PIL import Image
from tqdm import tqdm


def generate_primes(number: int) -> List[bool]:
    """
    Generate a list of prime numbers up to n.

    Parameters:
    n (int): The upper limit for generating prime numbers.

    Returns:
    list: A list of boolean values representing whether the index is a prime number.
    """
    # Initialize a list of boolean values representing prime numbers
    primes = [False, False] + [True] * (number-1)

    # Iterate over each number up to the square root of n
    for i in tqdm(range(2, int(math.sqrt(number)) + 1), desc="Generating primes"):
        # If the current number is prime
        if primes[i]:
            # Mark its multiples as not prime
            for j in range(i*i, number+1, i):
                primes[j] = False
    return primes

def create_image(shape: str, width: int, height: int, filename: str) -> None:
    """
    Create an image of a specified shape filled with prime numbers.

    Parameters:
    shape (str): The shape of the image ('circle' or 'rectangle').
    width (int): The width of the image.
    height (int): The height of the image.
    filename (str): The filename to save the image as.
    """
    # Create a new image with the specified width and height
    image = Image.new('RGBA', (width, height), (0, 0, 0, 0))

    # Load the pixel map of the image
    pixels = image.load()

    # Generate a list of prime numbers up to the total number of pixels in the image
    primes = generate_primes(width * height)

    num = 1

    # If the shape is a circle
    if shape == 'circle':
        # Calculate the radius and center coordinates of the circle
        radius = min(width, height) // 2
        center_x = width // 2
        center_y = height // 2

        # Iterate over each pixel in the image
        for coord_y in tqdm(range(height), desc="Creating image"):
            for coord_x in range(width):
                # Calculate the distance from the current pixel to the center of the circle
                diff_x = coord_x - center_x
                diff_y = coord_y - center_y

                # If the pixel is within the circle
                if diff_x*diff_x + diff_y*diff_y <= radius*radius:
                    # Color it based on whether its index is a prime number
                    pixels[coord_x, coord_y] = (
                        (173, 216, 230, 255) if primes[num] else (0, 0, 0, 255)
                    )
                    num += 1

    # If the shape is a rectangle
    elif shape == 'rectangle':
        # Iterate over each pixel in the image
        for coord_y in tqdm(range(height), desc="Creating image"):
            for coord_x in range(width):
                # Color it based on whether its index is a prime number
                pixels[coord_x, coord_y] = (
                    (173, 216, 230, 255) if primes[num] else (0, 0, 0, 255)
                )
                num += 1

    # Save the image with the specified filename
    image.save(filename)

if __name__ == "__main__":
    # Create an argument parser object
    parser = argparse.ArgumentParser(description='This script generates an image of a specified \
        shape (circle or rectangle) filled with prime numbers. The color of each pixel in the \
        image is determined by whether its index is a prime number. The user can specify the \
        shape, width, and height of the image.')

    # Add arguments for shape, width and height with default values
    parser.add_argument('--shape', type=str, default='rectangle',
                        help='Shape of the image (circle or rectangle)')
    parser.add_argument('--width', type=int, default=20480,
                        help='Width of the image')
    parser.add_argument('--height', type=int, default=10266,
                        help='Height of the image')

    # Parse command line arguments
    ARGS = parser.parse_args()

    # Call create_image function with parsed arguments and save it as an image file.
    create_image(shape=ARGS.shape.lower(),
                width=ARGS.width,
                height=ARGS.height,
                filename=f'{ARGS.shape}_{ARGS.width}x{ARGS.height}.png')
