# Prime Pixels

Prime Pixels is a Python script that generates an image of a specified shape (circle or rectangle) filled with prime numbers.\
The color of each pixel in the image is determined by whether its index is a prime number.\
The user can specify the shape, width, and height of the image through command line arguments.

## Functions

- `generate_primes(number: int) -> List[bool]`: Generates a list of prime numbers up to a given number.
- `create_image(shape: str, width: int, height: int, filename: str) -> None`: Creates an image of a specified shape (circle or rectangle) filled with prime numbers.

## Requirements

- Python 3.11
- PIL (Pillow)
- tqdm

Install this package using pip:

```bash
pip install pillow tqdm
```

## Usage

Run the script directly from the command line with arguments for the shape, width, and height of the image.

```bash
python prime_pixels.py --shape circle --width 500 --height 500
```

This will generate a 'circle_500x500.png' file in your current directory.

## Circle 20480 x 10266

![Image Description](circle_20480x10266.png)

